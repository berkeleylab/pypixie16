pixie16.ui.binary\_browser package
==================================

pixie16.ui.binary\_browser.app module
-------------------------------------

.. automodule:: pixie16.ui.binary_browser.app
   :members:
   :undoc-members:
   :show-inheritance:

pixie16.ui.binary\_browser.channel\_select module
-------------------------------------------------

.. automodule:: pixie16.ui.binary_browser.channel_select
   :members:
   :undoc-members:
   :show-inheritance:

pixie16.ui.binary\_browser.data\_model module
---------------------------------------------

.. automodule:: pixie16.ui.binary_browser.data_model
   :members:
   :undoc-members:
   :show-inheritance:

pixie16.ui.binary\_browser.trace\_analysis module
-------------------------------------------------

.. automodule:: pixie16.ui.binary_browser.trace_analysis
   :members:
   :undoc-members:
   :show-inheritance:
