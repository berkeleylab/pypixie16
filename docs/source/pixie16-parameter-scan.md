pixie16-parameter-scan
======================

This program can be used to scan one or two parameters.

During the scan the program takes MCA spectra for a certain time and
plots either a 1d or 2d plot where the amplitude is the integral of
the MCA spectrum.

This can be used to easily find optimal values for certain parameters.
