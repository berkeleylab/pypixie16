pixie16.ui package
==================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pixie16.ui.binary_browser
   pixie16.ui.coincidence

Submodules
----------

pixie16.ui.plot module
^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pixie16.ui.plot
   :members:
   :undoc-members:
   :show-inheritance:

pixie16.ui.widgets module
^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pixie16.ui.widgets
   :members:
   :undoc-members:
   :show-inheritance:

