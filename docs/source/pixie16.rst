pixie16 usage
=============

Pypixie16 provides python wrappers for the XIA SDK and on top of this,
convenient functions that make it easier in python to change settings
and to start and stop runs, as well as, to look at the data the
pixie16 generates.


Usage
-----

A quick introduction including example for some use cases:

.. toctree::
   :maxdepth: 1

   pixie16-change_settings
   pixie16-run

Executables
-----------

The following programs are included in pypixie16:

.. toctree::
   :maxdepth: 1

   pixie16-binary-browser
   pixie16-parameter-scan
   pixie16-coincidence

