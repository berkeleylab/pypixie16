pixie16-coincidence
===================

This app allows visualisation of coincidence patterns. By default is shows a 3 channel coincidence setting and one can modify the timing windows and by clicking in the top part of the window once can position the FastTrigger for each channel and see how the coincidence would work out.

You can also load a setting file in which case the program will use those settings to initialize some parameters.

Note: this is still work in progress, suggestions to improve the program are very welcome.
