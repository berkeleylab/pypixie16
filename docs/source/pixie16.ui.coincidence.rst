pixie16.ui.coincidence package
==============================

pixie16.ui.coincidence.app module
---------------------------------

.. automodule:: pixie16.ui.coincidence.app
   :members:
   :undoc-members:
   :show-inheritance:

pixie16.ui.coincidence.coincidence\_plots module
------------------------------------------------

.. automodule:: pixie16.ui.coincidence.coincidence_plots
   :members:
   :undoc-members:
   :show-inheritance:

pixie16.ui.coincidence.main\_widget module
------------------------------------------

.. automodule:: pixie16.ui.coincidence.main_widget
   :members:
   :undoc-members:
   :show-inheritance:
