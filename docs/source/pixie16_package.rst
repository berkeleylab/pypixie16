pixie16 package
===============

Documentation for each submodule, subpackage, and function in pypixiei16.

Submodules
----------

.. toctree::
   :maxdepth: 4

   pixie16.c-library-sdk
   pixie16.analyze
   pixie16.config
   pixie16.control
   pixie16.plot
   pixie16.scan
   pixie16.pipeline
   pixie16.tasks

Subpackages
-----------

Currently there are two subpackages for reading binary data such as
list mode data and settings data and another package for UI elements
that are used in the Qt-based interfaces provided to, for example,
look at binary data files.

.. toctree::
   :maxdepth: 4

   pixie16.read
   pixie16.ui

