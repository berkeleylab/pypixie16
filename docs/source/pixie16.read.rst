pixie16.read package
====================

Classes to read settings and list mode data.

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pixie16.read.list_mode
   pixie16.read.settings

